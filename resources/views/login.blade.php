@extends('layouts.base')
@section('conteudo')
  <div class="container flex ">
    <h5 class="row justify-content-center">Acceso</h5>
    <div class="row justify-content-center">
      <img src="imgs/download.png" id="imgUser" />
    </div>
    <!-- form action="http://10.100.16.33/logar.php" method="post" -->
    <form action="{{route('logar')}}" method="post" id="formLogin">
      @csrf
      <div class="form-group">
        <label for="usuario">Usuário:</label>
        <input type="text" class="form-control" id="usuario" name="usuario" placeholder="nombre de usuario">
      </div>

      <div class="form-group">
        <label for="senha">Contraseña:</label>
        <input type="password" class="form-control" id="senha" name="senha" placeholder="contraseña de usuario">
      </div>
      <button type="submit" class="btn btn-success">iniciar sesión en</button>
    </form>
  </div>
  @endsection
  